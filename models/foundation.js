const mongoose = require('mongoose');

let FoundationSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: String,
});

module.exports = mongoose.model('Foundation', FoundationSchema);