const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FundNavSchema = new mongoose.Schema({
  fundId: {
    type: Schema.Types.ObjectId,
    ref: "Fund",
    required: true
  },
  nav: {
    type: Number,
    required: true,
  },
  ma20: {
    type: Number,
    required: false,
    default: -1,
  },
  ma60: {
    type: Number,
    required: false,
    default: -1,
  },
  ma3072: {
    type: Number,
    required: false,
    default: -1,
  },
  div: {
    type: Number,
    required: false,
    default: -1,
  },
  lastUpdate: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model('FundNav', FundNavSchema);