const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FundSchema = new mongoose.Schema({
  cnyesId: {
    type: String,
    required: true,
  },
  displayNameLocal: {
    type: String,
    required: true,
  },
  investmentArea: {
    type: String,
    required: true,
  },
  classCurrency: {
    type: String,
    required: true,
  },
  categoryAbbr: {
    type: String,
    required: false,
  },
});

module.exports = mongoose.model('Fund', FundSchema);