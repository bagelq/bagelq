var mongoose = require('mongoose');
const axios = require('axios');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const Fund = require('./models/fund');
const FundNav = require('./models/fundnav');

const Alarm = require('./alarm');

var mongoDB = 'mongodb://192.168.2.18:27017/funds';

mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.Promise = global.Promise;

var db = mongoose.connection;

function delay(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, ms)
  });
}

function getDate(s) {
  return (new Date(s*1000)).toLocaleDateString();
}

function getDeltaDay(x) {
  return (1586188800-x)/86400;
}

function getDeltaSecond(d) {
  return (d * 86400);
}

function add(a, b) {
  return ((a*1000 + b*1000)/1000);
}

function divide(a, b) {
  return Number(((a*1000) / (b*1000)).toFixed(2));
}

async function getFundNavAndMa() {
  const now = Date.now();
  const fundList = await Fund.find({}).exec();
  for(let i=0;i<fundList.length;i++) {
  // for(let i=0;i<1;i++) {
    try {
      const fundNavs = await FundNav.find({fundId: ObjectId(fundList[i]._id)}).sort({lastUpdate: 'desc'}).limit(1).exec();
      const res = await axios.get(`https://fund.api.cnyes.com/fund/api/v1/funds/${fundList[i].cnyesId}/nav?&startAt=${Math.floor(new Date()/1000) - getDeltaSecond(120)}`);
      const navs = res.data.items.nav;
      const tradeDate = res.data.items.tradeDate;
      // console.log(`${i+1} ${res.data.items.displayNameLocal} ${navs.length} ${getDate(tradeDate[0])}`);

      if(fundNavs.length > 0 && fundNavs[0].lastUpdate === tradeDate[0]) {
        continue;
      }

      let ma20 = 0, ma30 = 0, ma72 = 0, ma60 = 0;

      for(let j=0;j<navs.length;j++) {
        if(j < 20) {
          ma20 = add(ma20, navs[j]);
        }

        if(j < 30) {
          ma30 = add(ma30, navs[j]);
        }

        if(j < 60) {
          ma60 = add(ma60, navs[j]);
        }

        if(j < 72) {
          ma72 = add(ma72, navs[j]);
        }
      }

      ma20 = divide(ma20, 20);
      ma30 = divide(ma30, 30);
      ma60 = divide(ma60, 60);
      ma72 = divide(ma72, 72);

      const ma3072 = divide(add(ma30, ma72), 2);
      const div = divide((navs[0] - ma3072) * 100, ma3072);
      // console.log(`nav ${navs[0]}, ma20 ${ma20}, ma60 ${ma60}, ma30+ma72/2 ${ma3072}, div ${div}`);

      await FundNav.create({
        fundId: ObjectId(fundList[i]._id),
        lastUpdate: tradeDate[0],
        nav: navs[0],
        ma20,
        ma60,
        ma3072,
        div
      });
    }
    catch(err) {
      console.error(err);
      console.log(`Update fail in ${(new Date()).toLocaleString()}`);
      return;
    }
  }

  console.log(`Complete in ${(Date.now()-now)/1000} seconds, ${(new Date()).toLocaleString()}`);
}

// getFundNavAndMa();

// every 6:30am
const poll = new Alarm(getFundNavAndMa, 6, 30);

module.exports = {
  get: (offset, count, sort) => {
    return new Promise( async (resolve, reject) => {
      offset = (offset) ? offset : 0;
      count = (count) ? count : 10;

      let funds = Fund.aggregate([
        {
          $lookup: {
            as: 'fundnavs',
            from: 'fundnavs',
            let: {
              "fundId": "$_id",
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ["$fundId", "$$fundId"],
                  }
                }
              },
              {
                $sort: {
                  "lastUpdate": -1,
                }
              },
              {
                $limit: 1,
              }
            ],
          },
        },
        {
          $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$fundnavs", 0 ] }, "$$ROOT" ] } }
        },
        { $project: { fundnavs: 0 } }
      ])

      if(sort) {
        const des = (sort === "-1") ? 1 : -1;
        funds = funds.sort({"div": des})
      }

      const list = await funds
      .skip(offset)
      .limit(count)
      .exec();

      const res = list.map(el => {
        return {
          fundId: el.cnyesId,
          fundName: el.displayNameLocal,
          nav: el.nav,
          ma3072: el.ma3072,
          div: el.div,
          lastUpdate: el.lastUpdate,
          currency: el.classCurrency,
          area: el.investmentArea,
        };
      });

      const total = await Fund.countDocuments().exec();

      resolve({list: res, num: total});
    });
  },
  close: () => {
    db.close();
  },
};


// test
// const testGet = Fund.aggregate([
//   {
//     $lookup: {
//       as: 'fundnavs',
//       from: 'fundnavs',
//       let: {
//         "fundId": "$_id",
//       },
//       pipeline: [
//         {
//           $match: {
//             $expr: {
//               $eq: ["$fundId", "$$fundId"],
//             }
//           }
//         },
//         {
//           $sort: {
//             "lastUpdate": -1,
//           }
//         },
//         {
//           $limit: 1,
//         }
//       ],
//     },
//   },
//   {
//     $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$fundnavs", 0 ] }, "$$ROOT" ] } }
//   },
//   { $project: { fundnavs: 0 } }
// ]).limit(1).exec();

// testGet.then(res => {
//   console.log(res);
// });