FROM node:12.16
LABEL maintainer="tsaihp@gmail.com"

RUN unlink /etc/localtime
RUN ln -s /usr/share/zoneinfo/Asia/Taipei /etc/localtime

WORKDIR /bagelq

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000
CMD [ "npm", "run", "start" ]